FROM node
ENV HOST_URL "http://127.0.0.1:8080"
WORKDIR /home/denner
COPY build build/.
RUN npm install -g http-server

ADD entrypoint.sh entrypoint.sh
RUN chmod 755 entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
