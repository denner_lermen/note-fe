import React, { Component } from 'react'
import { connect } from 'react-redux'
import { editorChangedTitle } from './editor.reducer'
import { Form } from 'react-bootstrap'
import './TitleEditor.scss'

class TitleEditor extends Component {
  constructor (props) {
    super(props)
    this.state = { title: '' }
  }

  componentWillReceiveProps (nextProps, nextContext) {
    const { note: oldNote = {} } = this.props
    const { note: newNote = {} } = nextProps

    if (newNote.id !== oldNote.id) {
      this.setState({ title: newNote.title })
    }
  }

  changeTitle (evt) {
    const { editorChangedTitle, note = {} } = this.props
    note.title = evt.target.value
    this.setState({ title: note.title })
    editorChangedTitle(note)
  }

  render () {
    return (
      <Form.Control type='text' placeholder='Title' className='title-input'
        value={this.state.title}
        onChange={this.changeTitle.bind(this)} />
    )
  }
}

const mapStateToProps = state => ({
  note: state.editorState.currentNote
})

const mapDispatchToProps = { editorChangedTitle }

export default connect(mapStateToProps, mapDispatchToProps)(TitleEditor)
