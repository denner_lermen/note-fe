import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Editor, RichUtils } from 'draft-js'
import SaveNoteButton from './SaveNoteButton'
import { editorTextChanged, updateEditorState } from './editor.reducer'
import TitleEditor from './TitleEditor'
import BlockStyleControls from './components/BlockStyleControls'
import InlineStyleControls from './components/InlineStyleControls'

class CreateNoteButton extends Component {
  getBlockStyle (block) {
    switch (block.getType()) {
      case 'blockquote':
        return 'RichEditor-blockquote'
      default:
        return null
    }
  }

  toggleBlockType (blockType) {
    const a = RichUtils.toggleBlockType(
      this.props.editorState,
      blockType
    )

    this.props.updateEditorState(a)
  }

  render () {
    const { editorTextChanged, note, editorState } = this.props
    return (
      <div>
        <TitleEditor />
        <BlockStyleControls />
        <InlineStyleControls />
        <Editor
          blockStyleFn={this.getBlockStyle.bind(this)}
          editorState={editorState}
          onChange={editorState => editorTextChanged(editorState, note)} />
        <SaveNoteButton />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  note: state.editorState.currentNote,
  editorState: state.editorState.editorState
})

const mapDispatchToProps = { editorTextChanged, updateEditorState }

export default connect(mapStateToProps, mapDispatchToProps)(CreateNoteButton)
