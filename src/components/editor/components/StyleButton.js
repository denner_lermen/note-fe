import React from 'react'
import './Editor.scss'

export class StyleButton extends React.Component {
  onToggle (e) {
    e.preventDefault()
    this.props.onToggle(this.props.style)
  }

  render () {
    let className = 'RichEditor-styleButton'
    if (this.props.active) {
      className += ' RichEditor-activeButton'
    }

    return (
      <span className={className} onMouseDown={this.onToggle.bind(this)}>
        {this.props.label}
      </span>
    )
  }
}
