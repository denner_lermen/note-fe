import React from 'react'
import './Editor.scss'
import { StyleButton } from './StyleButton'
import { toggleInlineStyle } from '../editor.reducer'
import { connect } from 'react-redux'

const INLINE_STYLES = [
  { label: 'Bold', style: 'BOLD' },
  { label: 'Italic', style: 'ITALIC' },
  { label: 'Underline', style: 'UNDERLINE' },
  { label: 'Monospace', style: 'CODE' }
]

class InlineStyleControls extends React.Component {
  render () {
    const { editorState, toggleInlineStyle } = this.props
    if (!editorState) {
      return ''
    }
    var currentStyle = editorState.getCurrentInlineStyle()
    return (
      <div className='RichEditor-controls'>
        {INLINE_STYLES.map(type =>
          <StyleButton
            key={type.label}
            active={currentStyle.has(type.style)}
            label={type.label}
            onToggle={(style) => toggleInlineStyle(style, editorState)}
            style={type.style}
          />
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  editorState: state.editorState.editorState
})

const mapDispatchToProps = { toggleInlineStyle }

export default connect(mapStateToProps, mapDispatchToProps)(InlineStyleControls)
