import { httpPut } from '../../config/restTemplate'
import { updateNoteTitle } from '../../pages/note/noteList.reducer'
import { EditorState, RichUtils } from 'draft-js'
import { stateToHTML } from 'draft-js-export-html'
import { stateFromHTML } from 'draft-js-import-html'

export const ACTION_TYPES = {
  OPEN_NOTE: 'noteEditor/OPEN_NOTE',
  TEXT_CHANGED: 'noteEditor/TEXT_CHANGED',
  TITLE_CHANGED: 'noteEditor/TITLE_CHANGED',
  SAVE_NOTE: 'noteEditor/SAVE_NOTE',
  SAVE_NOTE_SUCCESS: 'noteEditor/SAVE_NOTE_SUCCESS',
  SAVE_NOTE_FAIL: 'noteEditor/SAVE_NOTE_FAIL',

  UPDATE_EDITOR_STATE: 'noteEditor/UPDATE_EDITOR_STATE'
}

const initialState = {
  currentNote: undefined,
  hasChanges: false,
  loading: false,
  editorState: EditorState.createEmpty()
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.OPEN_NOTE:
      return {
        ...initialState,
        currentNote: action.payload.note,
        hasChanges: action.payload.note.hasChanges,
        editorState: action.payload.editorState
      }
    case ACTION_TYPES.SAVE_NOTE:
      return { ...state, loading: true, hasChanges: false }
    case ACTION_TYPES.SAVE_NOTE_SUCCESS:
      return { ...state, loading: false }
    case ACTION_TYPES.SAVE_NOTE_FAIL:
      return { ...state, loading: false, hasChanges: true }
    case ACTION_TYPES.TEXT_CHANGED:
      return { ...state, hasChanges: true, editorState: action.payload }
    case ACTION_TYPES.TITLE_CHANGED:
      return { ...state, hasChanges: true }
    case ACTION_TYPES.UPDATE_EDITOR_STATE:
      return { ...state, editorState: action.payload }
    default:
      return state
  }
}

//
// // Actions
//

export const editorTextChanged = (editorState, note) => async dispatch => {
  const text = stateToHTML(editorState.getCurrentContent())

  if (!note) {
    return
  }
  note.note = text
  note.hasChanges = true
  dispatch({ type: ACTION_TYPES.TEXT_CHANGED, payload: editorState })
}

export const updateEditorState = (editorState) => async dispatch => {
  dispatch({ type: ACTION_TYPES.UPDATE_EDITOR_STATE, payload: editorState })
}

export const editorChangedTitle = (note) => async (dispatch, getState) => {
  dispatch({ type: ACTION_TYPES.TITLE_CHANGED })
  updateNoteTitle(note)(dispatch, getState)
}

export const updateNote = (note) => async dispatch => {
  if (!note) {
    return
  }
  note.hasChanges = false
  dispatch({ type: ACTION_TYPES.SAVE_NOTE })
  httpPut(`v1/notes/${note.id}`, note).then(
    () => dispatch({ type: ACTION_TYPES.SAVE_NOTE_SUCCESS })
  )
}

export const openNote = (note) => async dispatch => {
  const contentState = stateFromHTML(note.note || '')
  const editorState = EditorState.createWithContent(contentState)
  dispatch({ type: ACTION_TYPES.OPEN_NOTE, payload: { note, editorState } })
}

export const toggleBlockType = (blockType, editorState) => async dispatch => {
  const newEditorState = RichUtils.toggleBlockType(
    editorState,
    blockType
  )
  updateEditorState(newEditorState)(dispatch)
}

export const toggleInlineStyle = (inlineStyle, editorState) => async dispatch => {
  console.log('inlineStyle', inlineStyle)
  const newEditorState = RichUtils.toggleInlineStyle(
    editorState,
    inlineStyle
  )
  updateEditorState(newEditorState)(dispatch)
}
