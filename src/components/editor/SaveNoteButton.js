import React, { Component } from 'react'
import Icon from '../icon/Icon'
import { connect } from 'react-redux'
import { Button } from 'react-bootstrap'
import { updateNote } from './editor.reducer'
import './SaveButtom.scss'

class SaveNoteButton extends Component {
  render () {
    return (
      <Button className='save-button' disabled={!this.props.hasChanges}
        onClick={() => this.props.updateNote(this.props.note)}
        variant='outline-primary'>
        <Icon icon='save' />
      </Button>
    )
  }
}

const mapStateToProps = state => ({
  note: state.editorState.currentNote,
  hasChanges: state.editorState.hasChanges
})

const mapDispatchToProps = { updateNote }

export default connect(mapStateToProps, mapDispatchToProps)(SaveNoteButton)
