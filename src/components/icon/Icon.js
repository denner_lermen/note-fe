import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome/index'
import { library } from '@fortawesome/fontawesome-svg-core/index'
import { fas } from '@fortawesome/free-solid-svg-icons/index'

library.add(fas)

class Icon extends Component {
  render () {
    return <FontAwesomeIcon style={this.props.style}
      className={this.props.className}
      icon={this.props.icon} />
  }
}

export default Icon
