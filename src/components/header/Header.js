import React, { Component } from 'react'
import { Navbar } from 'react-bootstrap'
import noteIcon from '../../assets/images/note.svg'
import './Header.scss'
import CreateNoteButton from '../../pages/note/CreateNoteButton'

class Header extends Component {
  render () {
    return (
      <Navbar bg='light' variant='light' className='justify-content-between'>
        <Navbar.Brand href='#home'>
          <img width='30'
            height='30'
            className='d-inline-block align-top'
            src={noteIcon} />
            Notes
        </Navbar.Brand>
        <CreateNoteButton />
      </Navbar>
    )
  }
}

export default Header
