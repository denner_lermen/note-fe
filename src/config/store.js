import { applyMiddleware, compose, createStore } from 'redux'
import reducer from './reducer'
import thunkMiddleware from 'redux-thunk'

const defaultMiddlewares = [
  thunkMiddleware
]

const composedMiddlewares = middlewares => compose(applyMiddleware(
  ...defaultMiddlewares, ...middlewares))

const initialize = (initialState, middlewares = []) => createStore(reducer,
  initialState, composedMiddlewares(middlewares))

export default initialize
