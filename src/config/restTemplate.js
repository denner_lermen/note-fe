const SERVER_URL = '/'

export const httpGet = (endpoint) => {
  return httpRequest(endpoint, 'GET')
}

export const httpPost = (endpoint, data) => {
  return httpRequest(endpoint, 'POST', data)
}

export const httpPut = (endpoint, data) => {
  return httpRequest(endpoint, 'PUT', data)
}

const httpRequest = (endpoint, method, body) => {
  return fetch(
    `${SERVER_URL}${endpoint}`,
    {
      method: method,
      headers: {
        'Content-Type': 'application/json'
      },
      body: body && JSON.stringify(body)
    }
  ).then(
    resp => resp.json())
}
