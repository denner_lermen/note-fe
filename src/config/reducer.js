import { combineReducers } from 'redux'
import noteListState from '../pages/note/noteList.reducer'
import editorState from '../components/editor/editor.reducer'

const rootReducer = combineReducers({
  noteListState,
  editorState
})

export default rootReducer
