import React, { Component } from 'react'
import './App.scss'
import { Col, Container, Row } from 'react-bootstrap'
import 'draft-js/dist/Draft.css'
import Header from './components/header/Header'
import NoteList from './pages/note/NoteList'
import Editor from './components/editor/Editor'
import initStore from './config/store'
import { Provider } from 'react-redux'

const store = initStore()

class App extends Component {
  render () {
    return (<Provider store={store}>
      <Container fluid>
        <Header />
        <Row className='content'>
          <Col sm={3} className='pr-0'>
            <NoteList />
          </Col>
          <Col sm={9} className='editor-content'>
            <Editor />
          </Col>
        </Row>
      </Container>
    </Provider>)
  }
}

export default App
