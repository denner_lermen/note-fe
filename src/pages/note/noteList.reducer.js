import { httpGet, httpPost } from '../../config/restTemplate'

const DEFAULT_NOTE_TITLE = 'Hello Word!'

export const ACTION_TYPES = {
  NOTE_LIST_LOAD: 'noteList/NOTE_LIST_LOAD',
  NOTE_LIST_LOAD_FAIL: 'noteList/NOTE_LIST_LOAD_FAIL',
  NOTE_LIST_LOAD_SUCCESS: 'noteList/NOTE_LIST_LOAD_SUCCESS'

}

const initialState = {
  loading: false,
  entities: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.NOTE_LIST_LOAD:
      return { ...initialState, loading: true }
    case ACTION_TYPES.NOTE_LIST_LOAD_SUCCESS:
      return { ...state, loading: false, entities: action.payload }
    case ACTION_TYPES.NOTE_LIST_LOAD_FAIL:
      return { ...state, loading: false }
    default:
      return state
  }
}

//
// // Actions
//

export const loadNotes = () => async dispatch => {
  httpGet('v1/notes?size=200').then(response => {
    dispatch(
      { type: ACTION_TYPES.NOTE_LIST_LOAD_SUCCESS, payload: response.content })
  }, () => {
    dispatch({ type: ACTION_TYPES.NOTE_LIST_LOAD_FAIL })
  })
}

export const createNote = () => async dispatch => {
  httpPost('v1/notes', { title: DEFAULT_NOTE_TITLE }).then(
    () => loadNotes()(dispatch)
  )
}

export const updateNoteTitle = (note) => async (dispatch, getState) => {
  const state = getState()
  const entities = state.noteListState.entities.map(item => {
    if (item.id === note.id) {
      return note
    }

    return item
  })

  dispatch({ type: ACTION_TYPES.NOTE_LIST_LOAD_SUCCESS, payload: entities })
}
