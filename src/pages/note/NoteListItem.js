import React, { Component } from 'react'
import Icon from '../../components/icon/Icon'
import { connect } from 'react-redux'
import { openNote } from '../../components/editor/editor.reducer'
import { ListGroup } from 'react-bootstrap'
import './NodeList.scss'

class NoteListItem extends Component {
  render () {
    const { note } = this.props
    return <ListGroup.Item action onClick={() => this.props.openNote(note)}
      key={note.id}>
      {note.title}
      <Icon className='note-list-item' icon='chevron-right' />
    </ListGroup.Item>
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = { openNote }

export default connect(mapStateToProps, mapDispatchToProps)(NoteListItem)
