import React, { Component } from 'react'
import Icon from '../../components/icon/Icon'
import { connect } from 'react-redux'
import { loadNotes } from './noteList.reducer'
import { openNote } from '../../components/editor/editor.reducer'
import { ListGroup } from 'react-bootstrap'
import './NodeList.scss'
import NoteListItem from './NoteListItem'

class NoteList extends Component {
  componentDidMount () {
    this.props.loadNotes()
  }

  noteItem (note) {
    return <ListGroup.Item action onClick={() => this.props.openNote(note)}
      key={note.id}>
      {note.title}
      <Icon className='note-list-item' icon='chevron-right' />
    </ListGroup.Item>
  }

  render () {
    const { entities } = this.props
    return (
      <ListGroup>
        {entities.map(note => <NoteListItem note={note} />)}
      </ListGroup>
    )
  }
}

const mapStateToProps = state => ({
  entities: state.noteListState.entities,
  loading: state.noteListState.loading
})

const mapDispatchToProps = { loadNotes, openNote }

export default connect(mapStateToProps, mapDispatchToProps)(NoteList)
