import React, { Component } from 'react'
import Icon from '../../components/icon/Icon'
import { connect } from 'react-redux'
import { createNote } from './noteList.reducer'
import { Button } from 'react-bootstrap'

class CreateNoteButton extends Component {
  render () {
    return (
      <Button onClick={this.props.createNote} variant='outline-primary'>
        <Icon icon='plus' />&nbsp;Create
      </Button>
    )
  }
}

const mapStateToProps = state => ({})

const mapDispatchToProps = { createNote }

export default connect(mapStateToProps, mapDispatchToProps)(CreateNoteButton)
